import React from "react";
import Form from "@rjsf/material-ui";
import { Button, Box } from "@material-ui/core";
import { schema } from "./schema";
import StepBar from "./StepBar";
import "./App.css";

const schemaData = schema.data;

const ObjectFieldTemplate = (props) => {
  return (
    <div>
      <div className="title">{props.title}</div>
      <div className="description">{props.description}</div>
      {props.properties.map((element) => (
        <div
          className={`property-wrapper ${
            element.content.props.uiSchema["ui:widget"] === "hidden"
              ? "hidden"
              : ""
          }`}
        >
          {element.content}
        </div>
      ))}
    </div>
  );
};

class App extends React.Component<any, any> {
  // Note: here used any type instead of the proper type just to build POC faster :)
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      formData: {},
      stepSchema: schemaData.length > 0 ? schemaData[0] : {},
    };
  }

  gotoNextStep = () => {
    const { step, formData } = this.state;
    let nextStep = this.getNextStep(step);
    if (nextStep > 0)
      this.setState({ step: nextStep, stepSchema: schemaData[nextStep - 1] });
    else {
      alert("You submitted " + JSON.stringify(formData, null, 2));
    }
  };

  gotoBackStep = () => {
    const { step } = this.state;
    let nextStep = this.getBackStep(step - 1);
    if (nextStep > 0)
      this.setState({ step: nextStep, stepSchema: schemaData[nextStep - 1] });
  };

  onSubmit = ({ formData }) => {
    const { step } = this.state;
    if (step !== schemaData.length) {
      this.setState(
        {
          formData: {
            ...this.state.formData,
            ...formData,
          },
        },
        this.gotoNextStep
      );
    } else {
      alert("You submitted " + JSON.stringify(formData, null, 2));
    }
  };

  getConditionResult = (condition) => {
    const { formData } = this.state;
    switch (condition.type) {
      case "Equal":
        return (
          formData[condition.compareTo] ==
          (condition.compare
            ? formData[condition.compare]
            : condition.compareValue)
        );
    }
    return false;
  };

  checkConditionallySkip = (step) => {
    let andCon = schemaData[step].conditionallySkip.$and;
    let orCon = schemaData[step].conditionallySkip.$or;
    let andRes = true;
    let orRes = true;
    if (andCon) {
      andCon.forEach((element) => {
        andRes = andRes && this.getConditionResult(element);
      });
    }
    if (orCon) {
      orCon.forEach((element) => {
        orRes = orCon || this.getConditionResult(element);
      });
    }
    return andRes && orRes;
  };

  getNextStep = (currentStep) => {
    if (currentStep === schemaData.length) {
      return 0;
    }
    if (schemaData[currentStep].conditionallySkip) {
      if (this.checkConditionallySkip(currentStep))
        return this.getNextStep(currentStep + 1);
      else return currentStep + 1;
    } else {
      return currentStep + 1;
    }
  };

  getBackStep = (currentStep) => {
    currentStep = currentStep - 1;
    if (currentStep === 0) {
      return 1;
    }
    if (schemaData[currentStep].conditionallySkip) {
      if (this.checkConditionallySkip(currentStep))
        return this.getBackStep(currentStep);
      else return currentStep + 1;
    } else {
      return currentStep + 1;
    }
  };

  diff = (obj1, obj2) => {
    //https://stackoverflow.com/questions/8431651/getting-a-diff-of-two-json-objects
    const result = {};
    if (Object.is(obj1, obj2)) {
      return undefined;
    }
    if (!obj2 || typeof obj2 !== "object") {
      return obj2;
    }
    Object.keys(obj1 || {})
      .concat(Object.keys(obj2 || {}))
      .forEach((key) => {
        if (obj2[key] !== obj1[key] && !Object.is(obj1[key], obj2[key])) {
          result[key] = obj2[key];
        }
        if (typeof obj2[key] === "object" && typeof obj1[key] === "object") {
          const value = this.diff(obj1[key], obj2[key]);
          if (value !== undefined) {
            result[key] = value;
          }
        }
      });
    return result;
  };

  getProps = (item) => {
    let newProps = {};
    Object.keys(item).forEach((element) => {
      if (item[element].properties) {
        let elementProp = this.getProps(item[element].properties);
        newProps = { ...newProps, ...elementProp };
      } else {
        newProps = { ...newProps, [element]: item[element] };
      }
    });
    return newProps;
  };

  combineProps = () => {
    let combinePropsResult = {};
    schemaData.forEach((item) => {
      let iProps = item.properties;
      let newProps = this.getProps(iProps);
      if (newProps) {
        combinePropsResult = { ...combinePropsResult, ...newProps };
      }
    });
    return combinePropsResult;
  };

  render() {
    const { step, stepSchema, formData } = this.state;
    let uiSchema: any = {};
    let combinePropsResult = this.combineProps();
    let diff = this.diff(stepSchema.properties, combinePropsResult);
    let stepSchemaRes = { ...stepSchema };
    stepSchemaRes.properties = {
      ...combinePropsResult,
      ...stepSchema.properties,
    };
    Object.keys(diff).forEach((item) => {
      if (diff[item])
        uiSchema = {
          ...uiSchema,
          [item]: {
            "ui:widget": "hidden",
          },
        };
    });

    return (
      <div className="App">
        <div className="step-wrapper">
          <StepBar totalStep={schemaData.length} currentStep={step} />
        </div>
        <Form
          schema={stepSchemaRes}
          onSubmit={this.onSubmit}
          formData={formData}
          uiSchema={uiSchema}
          ObjectFieldTemplate={ObjectFieldTemplate}
          noHtml5Validate
          showErrorList={false}
          key={step}
        >
          {step > 1 && (
            <Box display="inline" m={5}>
              <Button
                variant="contained"
                type="button"
                color="secondary"
                onClick={this.gotoBackStep}
              >
                Back
              </Button>
            </Box>
          )}
          <Box display="inline" m={5}>
            <Button variant="contained" color="primary" type="submit">
              {schemaData.length === step ? "Submit" : "Next"}
            </Button>
          </Box>
        </Form>
      </div>
    );
  }
}

export default App;
