export const schema = {
    data:
        [
            {
                title: "Personal Details",
                description: "A simple form example.",
                type: "object",
                required: ["firstName", "lastName"],
                properties: {
                    firstName: { type: "string", title: "First name", default: "Deep", },
                    lastName: {
                        type: "string",
                        title: "Last name"
                    },
                    residentialAddress: {
                        type: "object",
                        title: "Residential Address",
                        properties: {
                            residentialAddress1: {
                                type: "string",
                                title: "Address 1",
                            },
                            residentialAddress2: {
                                type: "string",
                                title: "Address 2",
                            }
                        }
                    },
                    presentAddress: {
                        type: "object",
                        title: "Present Address",
                        properties: {
                            sameAsRed: { type: "boolean", title: "Same as residential?", default: false }
                        },
                        dependencies: {
                            sameAsRed: {
                                oneOf: [
                                    {
                                        properties: {
                                            sameAsRed: {
                                                enum: [
                                                    false
                                                ]
                                            },
                                            presentAddress1: {
                                                type: "string",
                                                title: "Address 1",
                                            },
                                            presentAddress2: {
                                                type: "string",
                                                title: "Address 2",
                                            }
                                        }
                                    },
                                ]
                            }
                        }
                    },
                    addBeneficiary: { type: "boolean", title: "Do you want to add beneficiary?", default: false },
                },
            }, {
                title: "Beneficiary Details",
                type: "object",
                conditionallySkip: {
                    $and: [
                        {
                            type: "Equal",
                            compareValue: false,
                            compareTo: "addBeneficiary",
                        }
                    ]
                },
                properties: {
                    listOfStrings: {
                        type: "array",
                        title: "A list of beneficiary name",
                        items: {
                            type: "string",
                            default: "",
                            title: "Beneficiary name"
                        }
                    }
                }
            }, {
                title: "Country Details",
                type: "object",
                properties: {
                    country: {
                        type: "string",
                        title: "Country",
                        enum: [
                            "India", "Isreal"
                        ]
                    },
                    state: {
                        type: "string",
                        title: "State",
                        enum: ["Gujarat", "Rajasthan", "Jerusalem", "Ramla"]
                    },
                    city: {
                        type: "string",
                        title: "City",
                        enum: ["Ahmedabad", "Rajkot","Udaipur", "Jaipur","Jerusalem","Ramla"]
                    }
                },
                dependencies: {
                    country: {
                        oneOf: [
                            {
                                properties: {
                                    country: {
                                        enum: ['India']
                                    },
                                    state: {
                                        enum: ["Gujarat", "Rajasthan"]
                                    }
                                }
                            }, {
                                properties: {
                                    country: {
                                        enum: ['Isreal']
                                    },
                                    state: {
                                        enum: ["Jerusalem", "Ramla"]
                                    },
                                }
                            },
                        ]
                    },
                    state: {
                        oneOf: [
                            {
                                properties: {
                                    state: {
                                        enum: ['Gujarat']
                                    },
                                    city: {
                                        enum: ["Ahmedabad", "Rajkot"]
                                    }
                                }
                            }, {
                                properties: {
                                    state: {
                                        enum: ['Rajasthan']
                                    },
                                    city: {
                                        enum: ["Udaipur", "Jaipur"]
                                    },
                                }
                            }, {
                                properties: {
                                    state: {
                                        enum: ['Jerusalem']
                                    },
                                    city: {
                                        enum: ["Jerusalem"]
                                    },
                                }
                            }, {
                                properties: {
                                    state: {
                                        enum: ['Ramla']
                                    },
                                    city: {
                                        enum: ["Ramla"]
                                    },
                                }
                            }
                        ]
                    }
                }
            }, {
                title: "Payment Details",
                type: "object",
                properties: {
                    credit_card: { type: "number", title: "Credit Card" },
                    billing_address: { type: "string", title: "Billing address" },
                },
                dependencies: {
                    credit_card: [
                        "billing_address"
                    ]
                }
            }]
}