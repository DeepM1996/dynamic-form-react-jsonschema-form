import React from 'react';
import { LinearProgress, withStyles, Typography } from '@material-ui/core';

interface Props {
  totalStep: number;
  currentStep: number;
}

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 7,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor: theme.palette.grey[200],
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#ffa480',
  },
}))(LinearProgress);

const StepTextTypography = withStyles({
  root: {
    color: '#5b6192',
    fontSize: '17px',
    fontWeight: 600,
    letterSpacing: 1.2,
    marginBottom: '10px',
  },
})(Typography);

const StepBar = (props: Props) => {
  const { currentStep, totalStep } = props;
  const progress = (currentStep * 100) / totalStep;
  return (
    <div>
      <StepTextTypography>
        Step {currentStep}/{totalStep}
      </StepTextTypography>
      <BorderLinearProgress variant="determinate" value={progress} />
    </div>
  );
};

export default StepBar;
